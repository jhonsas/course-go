package main

import "fmt"

func main() {
	//var arrayX [10]int
	arrayX := [3]int{1, 2, 3}
	fmt.Println("Size: ", len(arrayX))
	fmt.Println(arrayX)

	for i := 0; i < len(arrayX); i++ {
		fmt.Println("Pos: ", i, " value: ", arrayX[i])
	}

	var matrix [2][3]int
	fmt.Println(matrix)

}
