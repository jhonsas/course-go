package main

import "fmt"

func main() {
	mentor := Mentor{Human{"Jhon"}, Dummy{"Sebastian"}}
	//fmt.Println(mentor.name)
	fmt.Println(mentor.Human.name)
	fmt.Println(mentor.Dummy.name)
	fmt.Println(mentor.talk())
}

type Human struct {
	name string
}

type Mentor struct {
	Human
	Dummy
}

type Dummy struct {
	name string
}

func (this Human) talk() string {
	return "La la la la"
}

func (this Mentor) talk() string {
	return "Welcome to Colombia"
}
