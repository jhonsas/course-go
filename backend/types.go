package main

import (
	"fmt"
	"strconv"
)

func main() {
	age := "10"
	age_int, _ := strconv.Atoi(age) // (_) will never be used, Atoi: convert string to int
	//age_str := strconv.Itoa(age_int) // Itoa: convert int to string
	fmt.Println(age_int + 9)
}
