package main

import "fmt"

func main() {
	var age int
	name := "Jhon" // defined value
	age = 19
	fmt.Println(name, " is ", age, " years old")
}
