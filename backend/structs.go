package main

import "fmt"

func main() {
	// var jhon User
	// jhon := User{name: "Jhon Sebastian", lastname: "Agudelo Sierra"}
	// jhon := User{19, "Jhon Sebastian", "Agudelo Sierra"}
	jhon := new(User) // Pointer, * is don't necessary
	jhon.name = "Jhon Sebastian"
	jhon.setName("Sebastian")
	jhon.lastname = "Agudelo Sierra"
	fmt.Println(jhon.complete_name())
}

// Type: defined new type
type User struct {
	age      int
	name     string
	lastname string
}

// *User: modify the structure
// User: copy of user
func (this User) complete_name() string {
	return this.name + " " + this.lastname
}

func (this *User) setName(name string) {
	this.name = name
}
