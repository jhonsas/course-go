package main

import "fmt"

func main() {
	var sum, size, numX int
	fmt.Scanf("%d", &size)
	for i := 0; i < size; i++ {
		fmt.Scanf("%d", &numX)
		sum += numX
	}
	fmt.Println(sum)
}
