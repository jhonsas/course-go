package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	var name string
	// %v || %s: concat string, %d: concat int, %t: concat boolean, %f: concat float
	fmt.Printf("-What is your name?\n")
	fmt.Scanf("%s\n", &name) // &: reference to memory address
	fmt.Printf("-Hi %s", name+"\n")
	fmt.Printf("-How old are you?\n")
	age, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("-Very nice, you have " + age + " years old")
	}
}
