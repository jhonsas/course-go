package main

import "fmt"

func main() {
	/*
		1. Is a memory direction
		2. we don't get the value, we get the address that contains the value
		3. X,Y => as123d => 5
		4. X => as123d => 6
		5. Y ¿? => 6
		USE:
		*T => *int, *string, *Struct
		Cero (0) value => nil
	*/

	var x, y *int
	integer := 5
	x = &integer
	y = &integer

	*x = 6 // *: access to value in memory

	fmt.Println(*x)
	fmt.Println(*y)
}
