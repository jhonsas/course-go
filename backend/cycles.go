package main

import "fmt"

func main() {
	i := 0
	fmt.Println("Default cycle")
	for i = 0; i < 10; i++ {
		fmt.Println("Hello World #", i)
	}
	i = 0
	fmt.Println("While cycle")
	for i < 10 {
		fmt.Println("i: ", i)
		i++
	}
	i = 0
	fmt.Println("Infinite cycle")
	for {
		if i == 2 {
			i++
			continue // back to the start of cycle
		}
		fmt.Println("i: ", i)
		i++
		if i > 10 {
			break
		}
	}
}
